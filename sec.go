package sec

import (
	"fmt"
	"log"

	"golang.org/x/crypto/bcrypt"
)

//HashAndSalt returns a stringified hash function
func HashAndSalt(input string) string {

	fmt.Println(input)
	pwd := []byte(input)
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}

	return string(hash)

}

//ComparePasswords checks if a given plaintext password matches a hash
func ComparePasswords(hashedPwd string, plainPwd []byte) bool {
	fmt.Println("Comparing \"", hashedPwd, "\" and \"", string(plainPwd), "\"")

	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		log.Println("Bcrypt Error:", err)
		return false
	}

	return true
}
